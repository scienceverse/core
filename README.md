# Construct Repository (CoRe)

This is a respository of constructs. Specifically, it hosts a number of Decentralized Construct Taxonomy specifications (DCTs). Such DCTs list, for a given construct, its definition and instructions for operationalisation and coding.

The repository is hosted at https://scienceverse.gitlab.io/core.
