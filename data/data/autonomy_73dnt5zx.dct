---
dct:
  version: 0.1.0
  id: autonomy_73dnt5zx
  label: "Autonomy"
  date: 2019-04-17
  source:
    label: "Fishbein, M. & Ajzen, I. (2010) Predicting and Changing Behavior: The Reasoned Action Approach. New York: Psychology Press."
    xdoi: "isbn:9781138995215"


################################################################################
    definition:
      definition: "People's perceptions of the degree of control they have over performing a given behavior."
      source: 
        spec: "p. 166"
################################################################################
    measure_dev:
      instruction: "Use Likert scales that measure the degree to which participants believe the target behavior to be under their control, for example by measuring their perceived  autonomy to perform the target behavior. The items suggested in the book are: 'Whether I [TARGET BEHAVIOR] is ...' with anchors 'Not up to me' vs 'Completely up to me' and 'For me to [TARGET BEHAVIOR] is under my control.' with anchors 'Not at all' vs 'Completely'."
      source:
        spec: "p. 462"
################################################################################
    measure_code: 
      instruction: "Questions or questionnaires that measure the perceived degree to which the target behavior or contrast behavior is under the control of a target population individual. For example, the items suggested in the book are: 'Whether I [TARGET BEHAVIOR] is ...' with anchors 'Not up to me' vs 'Completely up to me' and 'For me to [TARGET BEHAVIOR] is under my control.' with anchors 'Not at all' vs 'Completely'."
      source:
        spec: "p. 462"
################################################################################

  manipulate_dev:
    instruction: ""
    source:
      spec: ""

################################################################################

  manipulate_code:
    instruction: ""
    source:
      spec: ""

################################################################################
    aspect_dev:
      instruction: "Conduct a qualitative study where participants are interviewed, and the interviews are either recorded and transcribed, or notes are kept. These sources are then coded using the instruction for aspect coding. In this qualitative study, use these two questions. 'Please list any factors or circumstances that would make it easy or enable you to [target behavior].' and 'Please list any factors or circumstances that would make it difficult or prevent you from [target behavior].'"
      source:
        spec: p. 452
################################################################################
    aspect_code:
      instruction: "Expressions that demonstrate or imply presence of or lack of control over the target behavior because of, for example, a barrier, obstacle, or facilitating condition or circumstance. Expressions that reflect (lack of) confidence in one's *ability* to perform the target behavior should be coded as [dct:perceivedBehavioralControl_capacity_73bg9q7m]. Expressions that refer to confidence in general (not in one's *ability*), should be coded as [dct:perceivedBehavioralControl_71w8sfdk]."
################################################################################

  rel:
    id: "perceivedBehavioralControl_73dnt603"
    type: "structural_part_of"

################################################################################
---

