---
dct:
  version: 0.1.0
  id: expAttitude_expectation_73dnt5z1
  label: "Experiential attitude belief expectation"
  date: ""
  source:
    id: "raa_book"

################################################################################

  definition:
    definition: "An experiential attitude belief expectation is the expectation of how probable (i.e. unlikely versus likely) it is that engaging in the target behavior will cause one specific experiential potential consequence to come about. The experiential nature of this evaluation means that this concerns mostly expected experiences and sensations, such as pleasure or pain. Consequences that render the target behavior less or more instrumental given more long-term goals are captured in instrumental attitude and the underlying beliefs."
    source:
      spec: ""

################################################################################

  measure_dev:
    instruction: "To measure an experiential attitude belief expectation, first, identify exactly what potential experiential consequence of the target behavior (e.g. a specific experience or sensation) you want to measure. Then, establish whether accurately describing the spectrum of possibilities regarding this experiential consequence requires a unidimensional scale or a bidimensional scale. This depends on whether the 'default state' of this experiential consequence resembles absence of the experience, and therefore, engaging in the target behavior can only have an effect in one direction (requiring a unidimensional scale), or whether engaging in the target behavior can conceivably increase *or* decrease this experiential consequence. An example of the former, an experiential consequence that can be measured unidimensionally, is the experiential consequence of feeling less hungry after eating a pizza. The degree to which people will feel less hungry may vary; but it is excessively unlikely that somebody might feel that if they eat a pizza, they will then feel *more* hungry. An example of the latter, an experiential consequence that can be measured bidimensionally, is the experiential consequence of feeling relaxed after drinking alcohol. In this case, some people might feel less relaxed if they drink alcohol, while others might feel more relaxed.

For unidimensional scales, create an item stem that explicitly lists the experiential consequence, and makes clear that you are asking people about *their* expectation (not what they think holds more generally), and as anchors, use 'Very unlikely' and 'Very likely'. For example: 'If I eat pizza, it is ... that it will make me feel full. [Very unlikely|Very likely]'. For bidimensional scales, this is a bit more complicated, because the item stem cannot only list one of the two dimensions (e.g. 'feeling less relaxed' or 'feeling more relaxed'): people who would score low on the unidimensional scale might mean either that they don't think that consequence will occur, or that the opposite consequence will occur. Therefore, capturing the breadth of your target population's beliefs requires asking what they expect exactly. Therefore, the item stem contains the target behavior, and the anchors are the extremes of the bidimensional scale. For example, 'Drinking alcohol makes me feel ... [much less relaxed|much more relaxed]', or 'Drinking alcohol makes me feel ... [much more relaxed|much more excited]'.

In general, five-point scales are preferred; but for bidimensional scales, that only leaves two degrees of expression (the mid-point, after all, indicates no preference in bidimensional scales), so in that case, seven-point scales may be preferable. If both evaluations of unidimensional consequences and evaluations of bidimensional consequences are measured, either use two matrices or combine them in one, in which case you may want to use seven-point scales for all items. Try to always be consistent in the scale valence; in cultures that read from left to right, always place the most passive/low/weak scale extreme on the left, and the most active/high/strong scale extreme on the right. Do not reverse this order for one or more items."
    source:
      spec: ""

################################################################################

  measure_code:
    instruction: ""
    source:
      spec: ""

################################################################################

  manipulate_dev:
    instruction: ""
    source:
      spec: ""

################################################################################

  manipulate_code:
    instruction: ""
    source:
      spec: ""

################################################################################

  aspect_dev:
    instruction: "Conduct a qualitative study where participants are interviewed, and the interviews are either recorded and transcribed, or notes are kept. These sources are then coded using the instruction for aspect coding. In this qualitative study, use these questions: 'What do you see as the advantages of you [target behavior]?', 'What do you see as the disadvantages of you [target behavior]?', and 'What else comes to mind when you think about [target behavior]?'."
    source:
      spec: ""

################################################################################

  aspect_code:
    instruction: "Expressions of feelings, emotions, or affective associations that one expects to be caused by engaging or not engaging in the target behavior. An important distinction with instrumental attitude is that experiential attitude concerns acute goal-directed behavior, whereas instrumental attitude concerns longer-term goal-directed behavior. Experiential attitude reflects needs and urges that counter self-regulation and impulse control, whereas instrumental attitude reflects representations that promote self-regulation and impulse control. One's evaluation in terms of valence (i.e. positive versus negative) of the feelings, emotions, or affective associations should be coded as [dct:expAttitude_evaluation_73dnt5z2]."
    source:
      spec: ""

################################################################################

  rel:
    id: "expAttitude_belief_73dnt5z3"
    type: "causal_influences_product"

################################################################################
---

